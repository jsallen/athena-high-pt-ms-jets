// Getter tool(s):
#include "../GenEventGetterTool.h"
#include "../GenParticleGetterTool.h"
#include "../GenVertexGetterTool.h"
// Associator tool(s):
#include "../GenEventGenParticleAssociationTool.h"
#include "../GenParticleEventAssociationTool.h"
#include "../GenParticleParticleAssociationTool.h"
#include "../GenParticleVertexAssociationTool.h"
#include "../GenVertexParticleAssociationTool.h"
#include "../GenVertexEventAssociationTool.h"
#include "../TruthParticleChildAssociationTool.h"
#include "../TruthParticleParentAssociationTool.h"
#include "../TruthParticleProdVertexAssociationTool.h"
#include "../PileUpInfoAssociatorTool.h"
// Filler tool(s):
#include "../GenEventFillerTool.h"
#include "../GenEventPileUpFillerTool.h"
#include "../TruthParticleBremFillerTool.h"
#include "../GenParticleFillerTool.h"
#include "../GenParticlePerigeeFillerTool.h"
#include "../GenVertexFillerTool.h"
#include "../TruthParticleFillerTool.h"
#include "../PileUpInfoFillerTool.h"
#include "../TruthParticleFakerTool.h"
#include "../JetFullTruthTag.h"
#include "../TruthParticleClassificationFillerTool.h"
// Filter tool(s):
#include "../GenEventGetterFilterTool.h"

// Getter tool(s):
DECLARE_COMPONENT( D3PD::GenEventGetterTool )
DECLARE_COMPONENT( D3PD::GenParticleGetterTool )
DECLARE_COMPONENT( D3PD::GenVertexGetterTool )
// Associator tool(s):
DECLARE_COMPONENT( D3PD::GenEventGenParticleAssociationTool )
DECLARE_COMPONENT( D3PD::GenParticleEventAssociationTool )
DECLARE_COMPONENT( D3PD::GenParticleParticleAssociationTool )
DECLARE_COMPONENT( D3PD::GenParticleVertexAssociationTool )
DECLARE_COMPONENT( D3PD::GenVertexParticleAssociationTool )
DECLARE_COMPONENT( D3PD::GenVertexEventAssociationTool )
DECLARE_COMPONENT( D3PD::TruthParticleChildAssociationTool )
DECLARE_COMPONENT( D3PD::TruthParticleParentAssociationTool )
DECLARE_COMPONENT( D3PD::TruthParticleProdVertexAssociationTool )
DECLARE_COMPONENT( D3PD::PileUpInfoAssociatorTool )
// Filler tool(s):
DECLARE_COMPONENT( D3PD::GenEventFillerTool )
DECLARE_COMPONENT( D3PD::GenEventPileUpFillerTool )
DECLARE_COMPONENT( D3PD::TruthParticleBremFillerTool )
DECLARE_COMPONENT( D3PD::GenParticleFillerTool )
DECLARE_COMPONENT( D3PD::GenParticlePerigeeFillerTool )
DECLARE_COMPONENT( D3PD::GenVertexFillerTool )
DECLARE_COMPONENT( D3PD::TruthParticleFillerTool )
DECLARE_COMPONENT( D3PD::PileUpInfoFillerTool )
DECLARE_COMPONENT( D3PD::TruthParticleFakerTool )
DECLARE_COMPONENT( D3PD::JetFullTruthTag )
DECLARE_COMPONENT( D3PD::TruthParticleClassificationFillerTool )
// Filter tool(s):
DECLARE_COMPONENT( D3PD::GenEventGetterFilterTool )

